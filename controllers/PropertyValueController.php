<?php

namespace emilasp\taxonomy\controllers;

use emilasp\core\extensions\jsHelpers\JsHelpersAsset;
use emilasp\core\helpers\UrlHelper;
use emilasp\taxonomy\models\PropertyLink;
use Yii;
use emilasp\taxonomy\models\PropertyValue;
use emilasp\taxonomy\models\search\PropertyValueSearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PropertyValueController implements the CRUD actions for PropertyValue model.
 */
class PropertyValueController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index', 'view', 'create', 'update', 'delete', 'link-property', 'variated'],
                'rules'        => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'link-property', 'variated'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PropertyValue models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new PropertyValue();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //$model = new PropertyValue();
        }

        $searchModel = new PropertyValueSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'model'        => $model,
        ]);
    }

    /**
     * Displays a single PropertyValue model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, PropertyValue::className()),
        ]);
    }

    /**
     * Creates a new PropertyValue model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PropertyValue();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PropertyValue model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, PropertyValue::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PropertyValue model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, PropertyValue::className())->delete();

        return $this->redirect(['index']);
    }

    /**
     *  Добавляем вариацию для объекта
     */
    public function actionVariated()
    {
        $object     = Yii::$app->request->post('object');
        $objectId   = Yii::$app->request->post('object_id');
        $propertyId = Yii::$app->request->post('property_id');
        $link       = Yii::$app->request->post('link');

        $object = UrlHelper::classDecode($object);

        $object = $object::findOne($objectId);

        $saveLink = false;
        if ($link) {
            $object->variation_id = $propertyId;
        } else {
            $object->variation_id = null;
        }

        if ($object && $propertyId) {
            if ($object->save()) {
                $saveLink = true;
            }
        }

        echo JsHelpersAsset::response(
            $saveLink,
            Yii::t('taxonomy', 'Flash: Success save Property pseudo variation'),
            Yii::t('taxonomy', 'Flash: Error save Property  pseudo variation')
        );
    }

    /** Привязываем свойство к товару
     * @throws NotFoundHttpException
     */
    public function actionLinkProperty()
    {
        $object     = Yii::$app->request->post('object');
        $objectId   = Yii::$app->request->post('object_id');
        $propertyId = Yii::$app->request->post('property_id');
        $valueId    = Yii::$app->request->post('value_id');
        $link       = Yii::$app->request->post('link');

        $object = UrlHelper::classDecode($object);

        $object = $object::findOne($objectId);

        $saveLink = false;
        if ($link) {
            if ($object && $propertyId && $valueId) {
                if (PropertyLink::saveLink($propertyId, $valueId, $objectId, $object::className())) {
                    $saveLink = true;
                }
            }
        } else {
            if (PropertyLink::deleteLink($propertyId, $valueId, $objectId, $object::className())) {
                $saveLink = true;
            }
        }

        echo JsHelpersAsset::response(
            $saveLink,
            Yii::t('taxonomy', 'Flash: Success save Property Link'),
            Yii::t('taxonomy', 'Flash: Error create Property Link')
        );
    }

    /**
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionDeleteAjax()
    {
        $id = Yii::$app->request->post('id');
        if ($this->findModel($id, PropertyValue::className())->delete()) {
            $return[] = 1;
            $return[] = Yii::t('taxonomy', 'Flash: Success delete Property');
        } else {
            $return[] = 0;
            $return[] = Yii::t('taxonomy', 'Flash: Error delete Property');
        }

        echo Json::encode($return);
        Yii::$app->end();
    }
}
