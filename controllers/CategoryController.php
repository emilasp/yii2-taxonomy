<?php

namespace emilasp\taxonomy\controllers;

use Yii;
use emilasp\taxonomy\models\Category;
use emilasp\taxonomy\models\search\CategorySearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => [
                    'index',
                    'view',
                    'create',
                    'update',
                    'delete',
                    'moveNode',
                    'deleteNode',
                    'updateNode',
                    'createNode',
                ],
                'rules'        => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'moveNode',
                            'deleteNode',
                            'updateNode',
                            'createNode',
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            /*'moveNode'   => [
                'class'      => 'emilasp\taxonomy\extensions\nestedset\actions\MoveNodeAction',
                'modelClass' => Category::className(),
            ],
            'deleteNode' => [
                'class'      => 'emilasp\taxonomy\extensions\nestedset\actions\DeleteNodeAction',
                'modelClass' => Category::className(),
            ],
            'updateNode' => [
                'class'      => 'emilasp\taxonomy\extensions\nestedset\actions\UpdateNodeAction',
                'modelClass' => Category::className(),
            ],
            'createNode' => [
                'class'      => 'emilasp\taxonomy\extensions\nestedset\actions\CreateNodeAction',
                'modelClass' => Category::className(),
            ],*/
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex($rootId)
    {
        $root = Category::findOne($rootId);

        $modelId = Yii::$app->request->get('modelId', null);

        if ($modelId) {
            $model = Category::findOne($modelId);
        } else {
            $model = new Category();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->id) {
                $isSave = $model->update();
            } else {
                $isSave = $model->appendTo($root);
            }
            if ($isSave) {
                $return[] = 1;
                $return[] = 'Успешно';
            } else {
                $return[] = 0;
                $return[] = 'Не удалось сохранить';
            }
            echo Json::encode($return);
            Yii::$app->end();
        }

        $searchModel  = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
            'model'        => $model,
            'root'         => $root,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        if ($this->findModel($id, Category::className())->delete()) {
            $return[] = 1;
            $return[] = 'Успешно удалено';
        } else {
            $return[] = 0;
            $return[] = 'Не удалось удалить';
        }
        echo Json::encode($return);
        Yii::$app->end();
    }


    public function actionMoveNode()
    {
        $node       = (int)Yii::$app->request->post('node', null);
        $nodeTarget = (int)Yii::$app->request->post('nodeTarget', null);
        $mode       = Yii::$app->request->post('mode', null);

        /** @var Category $category */
        $category       = Category::findOne($node);
        $categoryTarget = Category::findOne($nodeTarget);

        switch ($mode) {
            case 'before':
                $category->insertBefore($categoryTarget);
                break;
            case 'after':
                $category->insertAfter($categoryTarget);
                break;
            case 'over':
                $category->appendTo($categoryTarget);
                break;
        }

        if ($category->hasErrors()) {
            $return[] = 0;
            $return[] = 'Шибка сохранения';
        } else {
            $return[] = 1;
            $return[] = 'Успешно';
        }

        echo Json::encode($return);
        Yii::$app->end();
    }

    public function actionCreateNode()
    {
        $node       = Yii::$app->request->post('node', null);
        $nodeTarget = Yii::$app->request->post('nodeTarget', null);
        $mode       = Yii::$app->request->post('mode', null);

        /** @var Category $category */
        $category       = Category::findOne($node);
        $categoryTarget = Category::findOne($nodeTarget);

        switch ($mode) {
            case 'before':
                $category->insertBefore($categoryTarget);
                break;
            case 'after':
                $category->insertAfter($categoryTarget);
                break;
            case 'over':
                $category->appendTo($categoryTarget);
                break;
        }

        if ($category->hasErrors()) {
            $return[] = 0;
            $return[] = 'Шибка сохранения';
        } else {
            $return[] = 1;
            $return[] = 'Успешно';
        }

        echo Json::encode($return);
        Yii::$app->end();
    }
}
