<?php

namespace emilasp\taxonomy\controllers;

use emilasp\taxonomy\models\PropertyValue;
use Yii;
use emilasp\taxonomy\models\Property;
use emilasp\taxonomy\models\search\PropertySearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PropertyController implements the CRUD actions for Property model.
 */
class PropertyController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index', 'view', 'create', 'update', 'delete'],
                'rules'        => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Property models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new PropertySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Property model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, Property::className()),
        ]);
    }

    /**
     * Creates a new Property model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Property();

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            Yii::$app->session->setFlash('success', Yii::t('taxonomy', 'Flash: Success create Property'));
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model'      => $model,
            'modelValue' => new PropertyValue(),
        ]);
    }

    /**
     * Updates an existing Property model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, Property::className());

        $modelValue = $this->createAjaxPropertyValue();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model'      => $model,
                'modelValue' => $modelValue,
            ]);
        }
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function createAjaxPropertyValue()
    {
        $dataValue = Yii::$app->request->post('PropertyValue', null);

        if ($dataValue && !empty($dataValue['id'])) {
            $model = $this->findModel($dataValue['id'], PropertyValue::className());
        } else {
            $model = new PropertyValue();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $return[] = 1;
                $return[] = Yii::t('taxonomy', 'Flash: Success save Property Value');
            } else {
                $return[] = 0;
                $return[] = Yii::t('taxonomy', 'Flash: Error create Property');
            }
            echo Json::encode($return);
            Yii::$app->end();
        }

        return $model;
    }

    /**
     * Deletes an existing Property model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, Property::className())->delete();

        return $this->redirect(['index']);
    }
}
