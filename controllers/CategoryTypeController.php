<?php

namespace emilasp\taxonomy\controllers;

use Yii;
use emilasp\taxonomy\models\Category;
use emilasp\taxonomy\models\search\CategorySearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CategoryTypeController implements the CRUD actions for Category model.
 */
class CategoryTypeController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => [
                    'index',
                    'view',
                    'create',
                    'update',
                    'delete',
                ],
                'rules'        => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Category model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, Category::className()),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->makeRoot()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, Category::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, Category::className())->delete();

        return $this->redirect(['index']);
    }


    public function actionMoveNode()
    {
        $node       = (int)Yii::$app->request->post('node', null);
        $nodeTarget = (int)Yii::$app->request->post('nodeTarget', null);
        $mode       = Yii::$app->request->post('mode', null);

        /** @var Category $category */
        $category       = Category::findOne($node);
        $categoryTarget = Category::findOne($nodeTarget);

        switch ($mode) {
            case 'before':
                $category->insertBefore($categoryTarget);
                break;
            case 'after':
                $category->insertAfter($categoryTarget);
                break;
            case 'over':
                $category->appendTo($categoryTarget);
                break;
        }

        if ($category->hasErrors()) {
            $return[] = 0;
            $return[] = 'Шибка сохранения';
        } else {
            $return[] = 1;
            $return[] = 'Успешно';
        }

        echo Json::encode($return);
        Yii::$app->end();
    }

    public function actionCreateNode()
    {
        $node       = Yii::$app->request->post('node', null);
        $nodeTarget = Yii::$app->request->post('nodeTarget', null);
        $mode       = Yii::$app->request->post('mode', null);

        /** @var Category $category */
        $category       = Category::findOne($node);
        $categoryTarget = Category::findOne($nodeTarget);

        switch ($mode) {
            case 'before':
                $category->insertBefore($categoryTarget);
                break;
            case 'after':
                $category->insertAfter($categoryTarget);
                break;
            case 'over':
                $category->appendTo($categoryTarget);
                break;
        }

        if ($category->hasErrors()) {
            $return[] = 0;
            $return[] = 'Шибка сохранения';
        } else {
            $return[] = 1;
            $return[] = 'Успешно';
        }

        echo Json::encode($return);
        Yii::$app->end();
    }
}
