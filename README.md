Модуль Taxonomy для Yii2
=============================

Модуль с таксономиями(категории, теги и т.п.)

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-taxonomy": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-taxonomy.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'taxonomy' => []
    ],
```
