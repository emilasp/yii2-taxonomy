<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151124_140302_AddProperty extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    public function up()
    {
        $this->createTable('taxonomy_property', [
            'id'               => $this->primaryKey(11),
            'name'             => $this->string(255)->notNull(),
            'type'             => $this->smallInteger(1)->notNull(),
            'filter_type_view' => $this->smallInteger(1),
            'value_as_page'    => $this->smallInteger(1),
            'is_variation'     => $this->smallInteger(1),
            'group_id'         => $this->integer(11),
            'status'           => $this->smallInteger(1)->notNull(),
            'created_at'       => $this->dateTime(),
            'updated_at'       => $this->dateTime(),
            'created_by'       => $this->integer(11),
            'updated_by'       => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_taxonomy_property_group_id',
            'taxonomy_property',
            'group_id',
            'taxonomy_property_group',
            'id'
        );

        $this->addForeignKey(
            'fk_taxonomy_property_created_by',
            'taxonomy_property',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_taxonomy_property_updated_by',
            'taxonomy_property',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addTypes();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('taxonomy_property');

        $this->afterMigrate();
    }


    private function addTypes()
    {
        Variety::add('property_type', 'property_type_value', 'Value', 1, 1);
        Variety::add('property_type', 'property_type_select', 'Select', 2, 2);
        Variety::add('property_type', 'property_type_color', 'Color', 3, 3);

        Variety::add('property_type_filter', 'property_type_filter_checkbox', 'Чекбокс', 1, 1);
        Variety::add('property_type_filter', 'property_type_filter_select', 'Select', 2, 2);
        Variety::add('property_type_filter', 'property_type_filter_select_search', 'Select+поиск', 3, 3);
        Variety::add('property_type_filter', 'property_type_filter_range', 'Range', 4, 4);
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
