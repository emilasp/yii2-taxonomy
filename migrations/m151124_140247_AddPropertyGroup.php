<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151124_140247_AddPropertyGroup extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('taxonomy_property_group', [
            'id'         => $this->primaryKey(11),
            'name'       => $this->string(255)->notNull(),
            'text'       => $this->text(),
            'order'      => $this->smallInteger(2),
            'view_type'  => $this->smallInteger(1),
            'visible'    => $this->smallInteger(1),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_taxonomy_property_group_created_by',
            'taxonomy_property_group',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_taxonomy_property_group_updated_by',
            'taxonomy_property_group',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addVisibles();
        $this->addViewTypes();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('taxonomy_property_group');

        $this->afterMigrate();
    }

    private function addVisibles()
    {
        Variety::add('property_group_visible', 'property_group_visible_yes', 'Да', 1, 1);
        Variety::add('property_group_visible', 'property_group_visible_no', 'Нет', 0, 2);
    }

    private function addViewTypes()
    {
        Variety::add('property_group_view_type', 'property_group_view_type_filter', 'Топ фильтр', 1, 1);
        Variety::add('property_group_view_type', 'property_group_view_type_sidebar', 'Сайдбар фильтр', 2, 2);
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
