<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151123_002739_AddLinkCategoryTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('taxonomy_link_category', [
            'id'         => $this->primaryKey(11),
            'object'      => $this->string(255)->notNull(),
            'object_id'       => $this->integer(11)->notNull(),
            'taxonomy_id'       => $this->integer(11)->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_taxonomy_link_category_taxonomy_id',
            'taxonomy_link_category',
            'taxonomy_id',
            'taxonomy_category',
            'id'
        );

        $this->createIndex('taxonomy_link_category_object', 'taxonomy_link_category', ['object', 'object_id']);
        $this->createIndex('taxonomy_link_category_taxonomy', 'taxonomy_link_category', ['taxonomy_id']);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('taxonomy_link_category');

        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
