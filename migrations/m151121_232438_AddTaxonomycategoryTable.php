<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151121_232438_AddTaxonomycategoryTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('taxonomy_category', [
            'id'             => $this->primaryKey(11),
            'code'           => $this->string(255)->unique(),
            'name'           => $this->string(255)->notNull(),
            'text'           => $this->text()->notNull(),
            'short_text'     => $this->text()->notNull(),
            'property_group' => 'jsonb NULL DEFAULT \'[]\'',
            'image_id'       => $this->integer(11),
            'type'           => $this->smallInteger(2)->notNull(),
            'status'         => $this->smallInteger(1)->notNull(),
            'tree'           => $this->integer(),
            'lft'            => $this->integer()->notNull(),
            'rgt'            => $this->integer()->notNull(),
            'depth'          => $this->integer()->notNull(),
            'created_at'     => $this->dateTime(),
            'updated_at'     => $this->dateTime(),
            'created_by'     => $this->integer(11),
            'updated_by'     => $this->integer(11),
        ], $this->tableOptions);

       /* $this->addForeignKey(
            'fk_taxonomy_category_image_id',
            'taxonomy_category',
            'image_id',
            'files_file',
            'id'
        );*/

        $this->addForeignKey(
            'fk_taxonomy_category_created_by',
            'taxonomy_category',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_taxonomy_category_updated_by',
            'taxonomy_category',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createIndex('taxonomy_category_type', 'taxonomy_category', 'type');

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('taxonomy_category');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
