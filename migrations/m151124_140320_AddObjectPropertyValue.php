<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151124_140320_AddObjectPropertyValue extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    public function up()
    {
        $this->createTable('taxonomy_property_value', [
            'id'          => $this->primaryKey(11),
            'value'       => $this->string(155)->notNull(),
            'data'        => $this->string(155),
            'order'       => $this->smallInteger(1),
            'property_id' => $this->integer(11)->notNull(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
        ], $this->tableOptions);

        $this->createTable('taxonomy_property_object_value', [
            'id'          => $this->primaryKey(11),
            'property_id' => $this->integer(11)->notNull(),
            'object'      => $this->string(155)->notNull(),
            'object_id'   => $this->integer(11)->notNull(),
            'value_id'    => $this->integer(11),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_taxonomy_property_value_property_id',
            'taxonomy_property_value',
            'property_id',
            'taxonomy_property',
            'id'
        );

        $this->addForeignKey(
            'fk_taxonomy_property_object_value_value_id',
            'taxonomy_property_object_value',
            'value_id',
            'taxonomy_property_value',
            'id'
        );

        $this->addForeignKey(
            'fk_taxonomy_property_object_value_property_id',
            'taxonomy_property_object_value',
            'property_id',
            'taxonomy_property',
            'id'
        );

        $this->createIndex(
            'taxonomy_property_object_value_property',
            'taxonomy_property_object_value',
            ['property_id', 'object', 'object_id']
        );

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('taxonomy_property_object_value');

        $this->afterMigrate();
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
