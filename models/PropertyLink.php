<?php

namespace emilasp\taxonomy\models;

use Yii;
use emilasp\core\components\base\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "taxonomy_property_object_value".
 *
 * @property integer $id
 * @property integer $property_id
 * @property string $object
 * @property integer $object_id
 * @property integer $value_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Property $property
 * @property PropertyValue $value
 * @property PropertyValueVariation[] $variations
 */
class PropertyLink extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxonomy_property_object_value';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'object', 'object_id'], 'required'],
            [['property_id', 'object_id', 'value_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['object'], 'string', 'max' => 155],
            [
                ['property_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Property::className(),
                'targetAttribute' => ['property_id' => 'id'],
            ],
            [
                ['value_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PropertyValue::className(),
                'targetAttribute' => ['value_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'property_id' => Yii::t('taxonomy', 'Property'),
            'object'      => Yii::t('site', 'Object'),
            'object_id'   => Yii::t('site', 'Object ID'),
            'value_id'    => Yii::t('taxonomy', 'Value'),
            'created_at'  => Yii::t('site', 'Created At'),
            'updated_at'  => Yii::t('site', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue()
    {
        return $this->hasOne(PropertyValue::className(), ['id' => 'value_id']);
    }


    /** Добавляем связь объекта со свойством и значением
     * @param $propertyId
     * @param $valueId
     * @param $objectClass
     * @param $objectId
     *
     * @return bool
     */
    public static function saveLink($propertyId, $valueId, $objectId, $objectClass)
    {
        $find = self::findOne([
            'property_id' => $propertyId,
            'value_id' => $valueId,
            'object_id' => $objectId,
            'object' => $objectClass,
        ]);

        if (!$find) {
            $link = new self();
            $link->property_id = $propertyId;
            $link->value_id = $valueId;
            $link->object_id = $objectId;
            $link->object = $objectClass;
            return $link->save();
        }
        return true;
    }

    /** Удаляем связь объекта со свойством и значением
     * @param $propertyId
     * @param $valueId
     * @param $objectClass
     * @param $objectId
     *
     * @return bool|false|int
     * @throws \Exception
     */
    public static function deleteLink($propertyId, $valueId, $objectId, $objectClass)
    {
        $find = self::findOne([
            'property_id' => $propertyId,
            'value_id' => $valueId,
            'object_id' => $objectId,
            'object' => $objectClass,
        ]);

        if ($find) {
            return $find->delete();
        }
        return false;
    }
}
