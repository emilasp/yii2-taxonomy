<?php

namespace emilasp\taxonomy\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\taxonomy\models\PropertyGroup;

/**
 * PropertyGroupSearch represents the model behind the search form about `emilasp\taxonomy\models\PropertyGroup`.
 */
class PropertyGroupSearch extends PropertyGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order', 'visible', 'view_type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['name', 'text', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PropertyGroup::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'order'      => $this->order,
            'visible'    => $this->visible,
            'view_type'  => $this->view_type,
            'status'     => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
