<?php

namespace emilasp\taxonomy\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\taxonomy\models\Category;
use yii\helpers\ArrayHelper;

/**
 * CategorySearch represents the model behind the search form about `emilasp\taxonomy\models\Category`.
 */
class CategorySearch extends Category
{
    public $search = true;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'image_id', 'type', 'status', 'tree', 'lft', 'rgt', 'depth', 'created_by', 'updated_by'], 'integer'],
            [['name', 'code', 'text', 'short_text', 'created_at', 'updated_at', 'property_group'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $roots = false)
    {
        $query = Category::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($roots) {
            $query->andFilterWhere([
                'id' => array_keys(ArrayHelper::map(self::getRoots(), 'id', 'name')),
            ]);
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'image_id' => $this->image_id,
            'type' => $this->type,
            'status' => $this->status,
            'tree' => $this->tree,
            'lft' => $this->lft,
            'rgt' => $this->rgt,
            'depth' => $this->depth,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'property_group', $this->property_group])
            ->andFilterWhere(['like', 'short_text', $this->short_text]);

        return $dataProvider;
    }
}
