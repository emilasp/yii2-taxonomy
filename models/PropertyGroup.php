<?php

namespace emilasp\taxonomy\models;

use emilasp\variety\behaviors\VarietyModelBehavior;
use emilasp\variety\models\Variety;
use Yii;
use emilasp\user\core\models\User;
use emilasp\core\components\base\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\DbDependency;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "taxonomy_property_group".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $order
 * @property integer $visible
 * @property integer $view_type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Property[] $properties
 * @property User $author
 * @property User $updatedBy
 */
class PropertyGroup extends ActiveRecord
{
    const VIEW_TYPE_TOP_FILTER     = 1;
    const VIEW_TYPE_SIDEBAR_FILTER = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxonomy_property_group';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status'    => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'variety_view_type' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'view_type',
                'group'     => 'property_group_view_type',
            ],
            'variety_visible'   => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'visible',
                'group'     => 'property_group_visible',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['text'], 'string'],
            [['order', 'visible', 'view_type', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'name'       => Yii::t('site', 'Name'),
            'text'       => Yii::t('site', 'Text'),
            'order'      => Yii::t('site', 'Order'),
            'visible'    => Yii::t('site', 'Visible'),
            'view_type'  => Yii::t('taxonomy', 'View type'),
            'status'     => Yii::t('site', 'Status'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'created_by' => Yii::t('site', 'Created By'),
            'updated_by' => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperties()
    {
        return $this->hasMany(Property::className(), ['group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /** Получаем группы
     * @return mixed
     */
    public static function getGroups()
    {
        $groups = self::getDb()->cache(
            function () {
                return self::findAll(['status' => Variety::getValue('status_enabled')]);
            },
            null,
            new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM ' . self::tableName()])
        );
        return $groups;
    }

    /** Проверяем что одно из переданных свойств есть в группе
     *
     * @param array $properties
     *
     * @return bool
     */
    public function hasProperties(array $properties)
    {
        foreach ($this->properties as $property) {
            if (in_array($property->id, $properties)) {
                return true;
            }
        }
        return false;
    }
}
