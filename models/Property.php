<?php

namespace emilasp\taxonomy\models;

use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use emilasp\user\core\models\User;
use emilasp\core\components\base\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "taxonomy_property".
 *
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property integer $filter_type_view
 * @property integer $value_as_page
 * @property integer $group_id
 * @property integer $is_variation
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $author
 * @property User $updatedBy
 * @property PropertyGroup $group
 * @property PropertyLink[] $propertyLinks
 */
class Property extends ActiveRecord
{
    const TYPE_VALUE  = 1;
    const TYPE_SELECT = 2;
    const TYPE_COLOR  = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxonomy_property';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_variation'   => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'is_variation',
                'group'     => 'bool',
            ],
            'variety_status'      => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'variety_type'        => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'property_type',
            ],
            'variety_type_filter' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'filter_type_view',
                'group'     => 'property_type_filter',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type', 'filter_type_view', 'status'], 'required'],
            [
                [
                    'type',
                    'filter_type_view',
                    'group_id',
                    'value_as_page',
                    'is_variation',
                    'status',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
            [
                ['group_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => PropertyGroup::className(),
                'targetAttribute' => ['group_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('site', 'ID'),
            'name'             => Yii::t('site', 'Name'),
            'type'             => Yii::t('site', 'Type'),
            'filter_type_view' => Yii::t('taxonomy', 'Filter Type View'),
            'value_as_page'    => Yii::t('taxonomy', 'Values as Page(SEO)'),
            'group_id'         => Yii::t('taxonomy', 'Group'),
            'is_variation'     => Yii::t('taxonomy', 'Variated'),
            'status'           => Yii::t('site', 'Status'),
            'created_at'       => Yii::t('site', 'Created At'),
            'updated_at'       => Yii::t('site', 'Updated At'),
            'created_by'       => Yii::t('site', 'Created By'),
            'updated_by'       => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(PropertyGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyLinks()
    {
        return $this->hasMany(PropertyLink::className(), ['property_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(PropertyValue::className(), ['property_id' => 'id'])->addOrderBy('value');
    }
}
