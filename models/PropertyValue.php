<?php
namespace emilasp\taxonomy\models;

use emilasp\seo\common\behaviors\SeoModelBehavior;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use emilasp\core\components\base\ActiveRecord;
use emilasp\variety\behaviors\VarietyModelBehavior;

/**
 * This is the model class for table "taxonomy_property_value".
 *
 * @property integer $id
 * @property string $value
 * @property string $data
 * @property string $property_id
 * @property integer $order
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PropertyLink[] $propertyLinks
 */
class PropertyValue extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxonomy_property_value';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_type' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'property_value_type',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class'         => SeoModelBehavior::className(),
                'route'         => '/taxonomy/property-value/view',
                'attributeName' => 'value',
                'attributeUnique' => 'id',
                'condition'     => [
                    'property' => [
                        'value_as_page' => true,
                    ],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'property_id'], 'required'],
            [['property_id', 'order'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['value', 'data'], 'string', 'max' => 155],

            [['title', 'title_h', 'keywords', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'value'       => Yii::t('taxonomy', 'Value'),
            'data'        => Yii::t('site', 'Data'),
            'type'        => Yii::t('site', 'Type'),
            'property_id' => Yii::t('taxonomy', 'Property'),
            'order'       => Yii::t('site', 'Order'),
            'created_at'  => Yii::t('site', 'Created At'),
            'updated_at'  => Yii::t('site', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPropertyLinks()
    {
        return $this->hasMany(PropertyLink::className(), ['value_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }
}
