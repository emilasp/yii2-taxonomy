<?php

namespace emilasp\taxonomy\models;

use emilasp\json\behaviors\JsonFieldBehavior;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use emilasp\user\core\models\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use emilasp\core\components\base\ActiveRecord;
use emilasp\seo\common\behaviors\SeoModelBehavior;
use emilasp\variety\behaviors\VarietyModelBehavior;

/**
 * This is the model class for table "taxonomy_category".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $text
 * @property string $short_text
 * @property string $property_group
 * @property integer $image_id
 * @property integer $type
 * @property integer $status
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Category extends ActiveRecord
{
    public $parent;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxonomy_category';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'nestedSetsBehavior' => [
                'class'         => \creocoder\nestedsets\NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
            ],
            'variety_status'     => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'seo'                => [
                'class' => SeoModelBehavior::className(),
                'route' => '/taxonomy/category/view',
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'property_group',
                'scheme'    => [
                    'group_id' => ['label' => 'group', 'rules' => []],
                ],
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'status'], 'required'],
            [['text', 'short_text'], 'string'],
            [['image_id', 'type', 'status', 'tree', 'lft', 'rgt', 'depth', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at', 'lft', 'rgt', 'depth', 'parent', 'property_group'], 'safe'],
            [['name', 'code'], 'string', 'max' => 55],

            [['title', 'title_h', 'keywords', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('site', 'ID'),
            'code'            => Yii::t('site', 'Code'),
            'name'            => Yii::t('site', 'Name'),
            'text'            => Yii::t('site', 'Text'),
            'short_text'      => Yii::t('site', 'Short Text'),
            'property_group'  => Yii::t('taxonomy', 'Property groups'),
            'image_id'        => Yii::t('site', 'Image ID'),
            'type'            => Yii::t('site', 'Type'),
            'status'          => Yii::t('site', 'Status'),
            'tree'            => Yii::t('taxonomy', 'Tree'),
            'lft'             => Yii::t('taxonomy', 'Lft'),
            'rgt'             => Yii::t('taxonomy', 'Rgt'),
            'depth'           => Yii::t('taxonomy', 'Depth'),
            'parent'          => Yii::t('site', 'Owner'),
            'created_at'      => Yii::t('site', 'Created At'),
            'updated_at'      => Yii::t('site', 'Updated At'),
            'created_by'      => Yii::t('site', 'Created By'),
            'updated_by'      => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /** Получаем главные категории(условно типы)
     * @return array|Category[]|\yii\db\ActiveRecord[]
     */
    public static function getRoots()
    {
        return self::find()->where('id IN (SELECT DISTINCT tree FROM ' . self::tableName() . ')')->all();
    }
}
