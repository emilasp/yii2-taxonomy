<?php

namespace emilasp\taxonomy\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use emilasp\core\components\base\BaseActiveQuery;

/**
 * This is the ActiveQuery class for [[Category]].
 *
 * @see Category
 */
class CategoryQuery extends BaseActiveQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }
}
