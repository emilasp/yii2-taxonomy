<?php

namespace emilasp\taxonomy\models;

use Yii;
use emilasp\core\components\base\ActiveRecord;

/**
 * This is the model class for table "taxonomy_link_category".
 *
 * @property integer $id
 * @property string $object
 * @property integer $object_id
 * @property integer $taxonomy_id
 */
class CategoryLink extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxonomy_link_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object', 'object_id', 'taxonomy_id'], 'required'],
            [['object_id', 'taxonomy_id'], 'integer'],
            [['object'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('site', 'ID'),
            'object'      => Yii::t('site', 'Object'),
            'object_id'   => Yii::t('site', 'Object ID'),
            'taxonomy_id' => Yii::t('taxonomy', 'Taxonomy'),
        ];
    }
}
