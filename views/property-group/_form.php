<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\PropertyGroup */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="property-group-form">

        <?php Pjax::begin(['id' => 'group-form-pjax']) ?>

        <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'visible')->dropDownList($model->visibles) ?>
        
        <?= $form->field($model, 'view_type')->dropDownList($model->view_types) ?>

        <?= $form->field($model, 'order')->textInput() ?>

        <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

        <?php Pjax::end(); ?>

    </div>
<?php
$this->registerJs(
    '$("document").ready(function(){
            $("#group-form-pjax").on("pjax:end", function() {
            $.pjax.reload({container:"#list-group-pjax"});
        });
    });'
);
?>