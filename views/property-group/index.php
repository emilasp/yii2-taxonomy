<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use emilasp\user\core\models\User;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\taxonomy\models\search\PropertyGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('taxonomy', 'Property Groups');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="property-group-index">

        <?php
        Modal::begin([
            'id'     => 'modal_category',
            'header' => '<h4>' . Yii::t('taxonomy', 'Group') . '</h4>',
        ]);
        echo Html::beginTag('div', ['class' => 'modal-content']);
        echo $this->render('_form', [
            'model' => $model,
        ]);
        echo Html::endTag('div');
        Modal::end();
        ?>

        <?php Pjax::begin(['id' => 'list-group-pjax']) ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                ['class' => '\kartik\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'class'     => '\kartik\grid\DataColumn',
                    'width'     => '100px',
                    'hAlign'    => GridView::ALIGN_CENTER,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                ],
                [
                    'attribute' => 'name',
                    'value'     => function ($model, $key, $index, $column) {
                        return Html::a($model->name, ['/taxonomy/property-group/update', 'id' => $model->id]);
                    },
                    'class'     => '\kartik\grid\DataColumn',
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'format'    => 'raw',
                ],
                'order',
                [
                    'attribute' => 'status',
                    'value'     => function ($model, $key, $index, $column) {
                        return $model->statuses[$model->status];
                    },
                    'class'     => '\kartik\grid\DataColumn',
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '150px',
                    'filter'    => $searchModel->statuses,
                ],
                [
                    'attribute'           => 'created_by',
                    'value'               => function ($model) {
                        return $model->createdBy->username;
                    },
                    'class'               => '\kartik\grid\DataColumn',
                    'hAlign'              => GridView::ALIGN_LEFT,
                    'vAlign'              => GridView::ALIGN_MIDDLE,
                    'width'               => '150px',
                    'filterType'          => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'language'      => \Yii::$app->language,
                        'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                        'options'       => ['placeholder' => '-выбрать-'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',
                ],
            ],
            'responsive'   => true,
            'hover'        => true,
            'condensed'    => true,
            'floatHeader'  => true,
            'panel'        => [
                'heading'    => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
                'type'       => 'info',
                'before'     =>
                    Html::button(Html::tag('i', '', ['class' => 'glyphicon glyphicon-plus']) . Yii::t('site', 'Add'), [
                        'class'     => 'btn btn-success btn-ajax-modal',
                        'data-pjax' => false,
                    ]),
                'after'      => Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                    ['index'],
                    ['class' => 'btn btn-info']
                ),
                'showFooter' => false,
            ],
        ]);
        ?>
        <?php Pjax::end() ?>

    </div>

<?php
$this->registerJs(<<<JS
    $('.btn-ajax-modal').click(function (){
        $('#modal_category').modal('show');
    });
JS
);