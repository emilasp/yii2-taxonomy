<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\PropertyGroup */

$this->title = Yii::t('taxonomy', 'Create Property Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Property Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-group-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
