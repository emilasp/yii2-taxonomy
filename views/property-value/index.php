<?php
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\taxonomy\models\search\PropertyValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('taxonomy', 'Property Values');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="property-value-index">

        <?php
        Modal::begin([
            'id'     => 'modal_category',
            'header' => '<h4>Category</h4>',
        ]);
        echo Html::beginTag('div', ['class' => 'modal-content']);
        echo $this->render('_form', [
            'model' => $model,
        ]);
        echo Html::endTag('div');
        Modal::end();
        ?>


        <?php Pjax::begin(['id' => 'list-values-pjax']) ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                ['class' => '\kartik\grid\SerialColumn'],
                [
                    'attribute' => 'id',
                    'class'     => '\kartik\grid\DataColumn',
                    'width'     => '100px',
                    'hAlign'    => GridView::ALIGN_CENTER,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                ],
                [
                    'attribute' => 'value',
                    'value'     => function ($model, $key, $index, $column) {
                        return Html::a($model->value, ['/taxonomy/property-value/update', 'id' => $model->id]);
                    },
                    'class'     => '\kartik\grid\DataColumn',
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'format'    => 'raw',
                ],
                'data',
                'property_id',
                [
                    'attribute' => 'type',
                    'value'     => function ($model, $key, $index, $column) {
                        $t = $model->types;
                        return $model->types[$model->type];
                    },
                    'class'     => '\kartik\grid\DataColumn',
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '150px',
                    'filter'    => $searchModel->types,
                ],
                [
                    'class' => '\kartik\grid\ActionColumn',
                ],
            ],
            'responsive'   => true,
            'hover'        => true,
            'condensed'    => true,
            'floatHeader'  => true,
            'panel'        => [
                'heading'    => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
                'type'       => 'info',
                'before'     => Html::button(Html::tag('i', '', ['class' => 'glyphicon glyphicon-plus']) . Yii::t('site', 'Add'), [
                    'class'       => 'btn btn-success btn-ajax-modal',
                    'data-pjax' => false,
                ]),
                'after'      => Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                    ['index'],
                    ['class' => 'btn btn-info']
                ),
                'showFooter' => false,
            ],
        ]);
        ?>
        <?php Pjax::end() ?>


    </div>

<?php
$this->registerJs(<<<JS
    $('.btn-ajax-modal').click(function (){
        $('#modal_category').modal('show');
    });
JS
);