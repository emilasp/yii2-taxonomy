<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\PropertyValue */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Property Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-value-view">

    <p>
        <?= Html::a(Yii::t('site', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('site', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('site', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'value',
            'data',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
