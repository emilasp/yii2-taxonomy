<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\PropertyValue */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('taxonomy', 'Property Value'),
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Property Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="property-value-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
