<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\PropertyValue */

$this->title = Yii::t('taxonomy', 'Create Property Value');
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Property Values'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-value-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
