<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use emilasp\seo\backend\widgets\SeoForm\SeoForm;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\PropertyValue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-value-form">

    <?php Pjax::begin(['id' => 'value-form-pjax']) ?>

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>



    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('taxonomy', 'Value') ?></a></li>
        <li><a data-toggle="tab" href="#seo"><?= Yii::t('seo', 'Tab Seo') ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="base" class="tab-pane fade in active">
            <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'data')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'property_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'order')->textInput() ?>
        </div>
        <div id="seo" class="tab-pane fade">
            <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>
<?php
$this->registerJs(
    '$("document").ready(function(){
            $("#value-form-pjax").on("pjax:end", function() {
            $.pjax.reload({container:"#list-values-pjax"});  //Reload GridView
        });
    });'
);
?>