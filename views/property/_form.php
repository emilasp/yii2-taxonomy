<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use emilasp\variety\models\Variety;
use emilasp\taxonomy\models\PropertyGroup;
use emilasp\taxonomy\extensions\PropList\PropList;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\Property */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="property-form">

    <div class="row">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'type')->dropDownList($model->types) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'filter_type_view')->dropDownList($model->filter_type_views) ?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'value_as_page')->checkbox() ?>
                </div>
            </div>

            <?= $form->field($model, 'group_id')
                     ->dropDownList(ArrayHelper::map(PropertyGroup::getGroups(), 'id', 'name')) ?>

            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
            <?= $form->field($model, 'is_variation')->dropDownList($model->is_variations) ?>

            <div class="form-group">
                <?= Html::submitButton(
                    $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
                ) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

        <div class="col-md-4">
            <?= PropList::widget([
                'modelValue' => $modelValue,
                'model'      => $model,
            ]) ?>
        </div>
    </div>
</div>
