<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\Property */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('taxonomy', 'Property'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Properties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="property-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelValue' => $modelValue,
    ]) ?>

</div>
