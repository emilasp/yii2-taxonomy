<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\Property */

$this->title = Yii::t('taxonomy', 'Create Property');
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Properties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelValue' => $modelValue,
    ]) ?>

</div>
