<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use emilasp\taxonomy\extensions\nestedTree\NestedTree;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\taxonomy\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('taxonomy', 'Categories') . ' ' . $root->name;
$this->params['breadcrumbs'][] =[ 'label' => Yii::t('taxonomy', 'Type Category'), 'url' => '/taxonomy/category-type'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <div class="row">
        <div class="col-md-3">
            <div>
                <button class="btn btn-success" id="add-category">
                    <i class="glyphicon glyphicon-plus"></i>
                    <?= Yii::t('site', 'Add')?>
                </button>
            </div>
            <?php Pjax::begin(['id'=>'tree-category-pjax']); ?>
            <?= NestedTree::widget([
                'id' => 'tree',
                'rootId' => $root->id,
                'options' =>[
                    'id' => 'tree',
                    'minExpandLevel' => '10',
                    'extensions' => ['dnd'],
                    //, 'table' http://goobbe.com/issue/7499592/fancytree-has-empty-nodes
                    'activate' => new JsExpression('function(event, data){
                            var node = data.node;
                            updateForm(node.key);
                    }'),
                    'dnd' => [
                        'preventVoidMoves' => true,
                        'preventRecursiveMoves' => true,
                        'autoExpandMS' => 400,

                        'dragStart' => new JsExpression('function(node, data) {
                            return true;
                        }'),
                        'dragEnter' => new JsExpression('function(node, data) {
                           return true;
                        }'),
                        'dragDrop' => new JsExpression('function(node, data) {
                            data.otherNode.moveTo(node, data.hitMode);
                            $.ajax({
                                type: "POST",
                                url: "/taxonomy/category/move-node",
                                dataType: "json",
                                data: "node="+data.otherNode.key+"&nodeTarget="+data.node.key+"&mode="+data.hitMode,
                                success: function(msg) {
                                    if(msg[0]=="1"){
                                        notice(msg[1], "green");
                                        /*$.pjax({
                                            container:"#tree-category-pjax",
                                            "timeout" : 0,
                                            //url: "/strategic/task/update-ajax",
                                            push:false
                                        });*/
                                    }else{
                                        notice(msg[1], "red");
                                    }
                                },
                                error: function(){}
                            });
                        }'),
                    ],
                ]
            ]);
            ?>
            <?php Pjax::end(); ?>
        </div>
        <div class="col-md-9">
            <?php Pjax::begin(['id'=>'form-category','scrollTo'=>true]); ?>
            <?= $this->render('_form', ['action' => 'update', 'model' => $model]) ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<?php
$jsTree =   <<<JS
$('body').on('click', '#add-category', function() {
    updateForm('');
});

function updateForm(id)
{
    $.pjax.reload({
        container:"#form-category",
        "timeout" : 0,
        //url: "/strategic/task/update-ajax",
        push:false,
        "data":{"modelId":id}
    });
}
JS;
$this->registerJs($jsTree, \yii\web\View::POS_END);