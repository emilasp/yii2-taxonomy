<?php
use emilasp\json\widgets\DynamicFields\DynamicFields;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use emilasp\seo\backend\widgets\SeoForm\SeoForm;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin([
        'id' => 'category-form',
    ]); ?>

    <div class="col-md-9">
        <div class="tab-content">
            <div id="base" class="tab-pane fade in active">
                <?php /* $form->field($model, 'type')->dropDownList($model::$types) */?>
                <?= $form->field($model, 'parent')->hiddenInput(['id' => 'cat_parent_id'])->label(false) ?>
                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                    'options' => ['rows' => 3],
                    'preset'  => 'standart',
                ]) ?>
                <?= $form->field($model, 'short_text')->widget(CKEditor::className(), [
                    'options' => ['rows' => 3],
                    'preset'  => 'standart',
                ]) ?>

                <?= DynamicFields::widget([
                    'form'      => $form,
                    'model'     => $model,
                    'attribute' => 'property_group',
                    'scheme'      => DynamicFields::TYPE_CUSTOM,
                    'viewFile'  => '@vendor/emilasp/yii2-taxonomy/views/category/json/_groups',
                ]); ?>

                <?php //@TODO добавить загрузку изображения ?>
                <?= $form->field($model, 'image_id')->textInput() ?>
                <?php
                $status = $model->statuses;
                ?>
                <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
            </div>
            <div id="seo" class="tab-pane fade">
                <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <ul class="nav nav-pills nav-stacked">
            <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('site', 'Tab Base') ?></a></li>
            <li><a data-toggle="tab" href="#seo"><?= Yii::t('seo', 'Tab Seo') ?></a></li>
            <li>
                <div class="row">
                    <div class="col-md-6">
                        <?= Html::submitButton(
                            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
                            ['class' => 'btn btn-success']
                        ) ?>
                    </div>
                    <div class="col-md-6 text-right">
                        <?= Html::button(
                            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Delete'),
                            [
                                'id'      => 'category-delete',
                                'data-id' => $model->id,
                                'class'   => 'btn btn-danger',
                            ]
                        ) ?>
                    </div>
                </div>

            </li>
        </ul>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$urlDelete = Url::toRoute(['/taxonomy/category/delete', 'id' => $model->id]);
$jsTree =   <<<JS
$(document).ready(function() {

    $('body').on('click', '#category-delete', function() {
        if (confirm("Удалить?")) {
            $.ajax({
                url: '{$urlDelete}',
                type: 'post',
                dataType: "json",
                success: function(data) {
                    if(data[0]==1) {
                        notice(data[1], 'green');
                        $.pjax({
                            container:"#tree-category-pjax",
                            "timeout" : 0,
                            push:false
                        });
                    } else {
                        notice(data[1], 'red');
                    }
                }
            });
        }
    });

    $('#category-form').on('beforeSubmit', function(event, jqXHR, settings) {
        var form = $(this);
        if(form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            dataType: "json",
            data: form.serialize(),
            success: function(data) {
             console.log(data[0]);
                if(data[0]==1) {
                    notice(data[1], 'green');
                    $.pjax({
                        container:"#tree-category-pjax",
                        "timeout" : 0,
                        //url: "/strategic/task/update-ajax",
                        push:false
                    });
                } else {
                    notice(data[1], 'red');
                }

            }
        });

        return false;
    })
});
JS;
$this->registerJs($jsTree);