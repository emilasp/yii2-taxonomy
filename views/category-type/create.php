<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\Category */

$this->title = Yii::t('taxonomy', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('taxonomy', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
