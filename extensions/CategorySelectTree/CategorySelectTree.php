<?php
namespace emilasp\taxonomy\extensions\CategorySelectTree;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\widgets\InputWidget;
use yii\helpers\ArrayHelper;
use emilasp\taxonomy\extensions\nestedTree\NestedTree;

/**
 * Class CategorySelectTree
 * @package emilasp\taxonomy\extensions\CategorySelectTree
 */
class CategorySelectTree extends InputWidget
{
    /** @var string рутовый итем */
    public $id = 'tree';

    /** @var string рутовый итем */
    public $rootId;

    public $fieldId = 'category';

    public $attribute;

    public $model;

    public $options = [];

    /** @var array */
    private $defaultOptions = [
        'id'             => 'tree',
        'extensions' => ['glyph'],
        'glyph'          => '{
                                map: {
                                  doc: "glyphicon glyphicon-file",
                                  docOpen: "glyphicon glyphicon-file",
                                  checkbox: "glyphicon glyphicon-unchecked",
                                  checkboxSelected: "glyphicon glyphicon-check",
                                  checkboxUnknown: "glyphicon glyphicon-share",
                                  dragHelper: "glyphicon glyphicon-play",
                                  dropMarker: "glyphicon glyphicon-arrow-right",
                                  error: "glyphicon glyphicon-warning-sign",
                                  expanderClosed: "glyphicon glyphicon-plus-sign",
                                  expanderLazy: "glyphicon glyphicon-plus-sign",
                                  expanderOpen: "glyphicon glyphicon-minus-sign",
                                  folder: "glyphicon glyphicon-folder-close",
                                  folderOpen: "glyphicon glyphicon-folder-open",
                                  loading: "glyphicon glyphicon-refresh"
                                }
                              }',
        'checkbox'       => true,
        'selectMode'     => 2,
        'minExpandLevel' => '10',
        'init'           => 'function(event) {
                var val = $("#{{selectorFiled}}-input").val();
                var ids = (val !== "" ? $.parseJSON(val) : {});
                $(event.target).fancytree("getTree").visit(function(node){
                    var checked = false;
                    $.map(ids, function(element, index) {
                       if (element == node.key) {
                           checked = true;
                       }
                    });
                    if (checked) {
                        node.setSelected(true);
                    }
                });
            }',
        'select'         => 'function(event, data) {
                var selNodes = data.tree.getSelectedNodes();
				var selKeys = $.map(selNodes, function(node){
					   return node.key;
					});
				$("#{{selectorFiled}}-input").val(JSON.stringify(selKeys));
            }',
        'click'          => 'function(event, data) {
                if( ! data.node.folder ){
                    data.node.toggleSelected();
                }
            }',
        'dblclick'       => 'function(event, data) {
                data.node.toggleExpanded();
            }',
        'keydown'        => 'function(event, data) {
                if( event.which === 32 ) {
                    data.node.toggleSelected();
                    return false;
                }
            }',
    ];

    /** @var array события fancyTree */
    public static $events = ['init', 'select', 'click', 'dblclick', 'keydown', 'glyph'];

    public function init()
    {
        parent::init();
        $this->prepareOptions();
    }

    public function run()
    {
        echo NestedTree::widget([
            'id'      => $this->id,
            'rootId'  => $this->rootId,
            'options' => $this->defaultOptions,
        ]);
        echo Html::activeInput('text', $this->model, $this->attribute, ['id' => $this->id . '-input']);
    }

    /**
     * Оборачиваем JS и устанавливаем в него id selector для инпута
     */
    private function prepareOptions()
    {
        foreach ($this->defaultOptions as $index => $option) {
            if (in_array($index, self::$events)) {
                $option                       = str_replace('{{selectorFiled}}', $this->id, $option);
                $this->defaultOptions[$index] = new JsExpression($option);
            }
        }
    }
}
