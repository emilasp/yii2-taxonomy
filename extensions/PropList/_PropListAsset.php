<?php
namespace emilasp\taxonomy\extensions\PropList;

use yii\web\AssetBundle;

/**
 * Class PropList
 * @package emilasp\taxonomy\extensions\PropList
 */
class PropListAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js  = ['nestedtree.js'];
    public $css = ['nestedtree.css'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'emilasp\taxonomy\extensions\nestedTree\TreeAsset',
    ];
}
