<?php
namespace emilasp\taxonomy\extensions\PropList;

use emilasp\taxonomy\extensions\FormValue\FormValue;
use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use emilasp\taxonomy\models\PropertyValue;
use dosamigos\selectize\SelectizeTextInput;

/**
 * Class PropList
 * @property mixed typeView
 * @package emilasp\taxonomy\extensions\PropList
 */
class PropList extends Widget
{
    const TYPE_LIST = 'list';

    public $id = 'propList';

    public $model;
    public $propertyId;
    public $modelValue;

    public $typeView = self::TYPE_LIST;

    public function init()
    {
        parent::init();
        if ($this->model) {
            $this->propertyId = $this->model->id;
        }
    }

    public function run()
    {
        $dataProvider = $this->getDataProvider();
        if ($this->typeView === self::TYPE_LIST) {
            echo $this->render('list', [
                'id' => $this->id,
                'model' => $this->model,
                'dataProvider' => $dataProvider,
                'modelValue' => $this->modelValue,
                'propertyId' => $this->propertyId
            ]);
        }
    }

    /** Получаем дата провайдер с значениями свойства
     * @return ActiveDataProvider
     */
    private function getDataProvider()
    {
        $condition = [];

        //if ($this->propertyId) {
            $condition['property_id'] = $this->propertyId;
        //}

        $query = PropertyValue::find()->where($condition);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_ASC]],
            'pagination' => [
                'pageSize' => 10,
                //'pageParam' => 'page',
                'validatePage' => false,
            ],
        ]);
        return $dataProvider;
    }
}
