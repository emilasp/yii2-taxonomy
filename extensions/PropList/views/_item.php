<?php use yii\helpers\Url; ?>

    <tr>
        <td>
            <a href="<?= Url::toRoute(['/taxonomy/property-value/update', 'id' => $model->id]) ?>" data-pjax="0">
                <?= $model->value ?>
            </a>
        </td>
        <td>
            <?= $model->data ?>
        </td>
        <td>
            <?= $model->created_at ?>
        </td>
        <td>
            <button class="btn btn-success btn-xs value-update"
                    data-id="<?= $model->id ?>"
                    data-value="<?= $model->value ?>"
                    data-data="<?= $model->data ?>"
                    data-propertyid="<?= $model->property_id ?>"
            >
                <span class="fa fa-pencil"></span>
            </button>
            <button class="btn btn-danger btn-xs value-delete" data-id="<?= $model->id ?>">
                <span class="fa fa-remove"></span>
            </button>
        </td>
    </tr>
