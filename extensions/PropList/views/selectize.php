<?php
use dosamigos\selectize\SelectizeTextInput;

echo SelectizeTextInput::widget([
    'name' => $this->id,
    'value' => 'love, this, game',
    'clientOptions' => [
        // ...
    ],
]);
