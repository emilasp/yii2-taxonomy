<?php
use emilasp\taxonomy\extensions\FormValue\FormValue;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ListView;

?>

<?= Html::button(Yii::t('taxonomy', 'Add Property Value'), [
    'id'       => $id . '-add',
    'class'    => 'btn btn-success',
    'disabled' => $model->isNewRecord,
]) ?>

<?php Pjax::begin(['id' => 'pjax-value-list']); ?>
    <table class="table">
        <tbody>
        <tr>
            <td><?= Yii::t('taxonomy', 'Value') ?></td>
            <td><?= Yii::t('taxonomy', 'Data') ?></td>
            <td><?= Yii::t('site', 'Created At') ?></td>
            <td></td>
        </tr>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions'  => ['class' => 'item'],
            'itemView'     => function ($model, $key, $index, $widget) {
                return $this->render('_item', ['model' => $model]);
            },
            'pager' => ['maxButtonCount' => 4]
        ]) ?>
        </tbody>
    </table>

<?php Pjax::end(); ?>

<?= FormValue::widget(['model' => $modelValue, 'propertyId' => $model->id]) ?>



<?php
$js = <<<JS
$('#{$id}-add').click(function () {
    $('#propertyvalue-id').val('');
    $('#propertyvalue-value').val('');
    $('#propertyvalue-data').val('');
});
    $('#{$id}-add').jBox('Modal', {
        title: 'Add',
        content: $('.property-value-form')
    });
JS;
$this->registerJs($js);

