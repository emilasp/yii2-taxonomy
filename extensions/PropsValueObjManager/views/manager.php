<?php
use emilasp\core\helpers\UrlHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use emilasp\taxonomy\models\PropertyGroup;
use emilasp\core\extensions\Sortable\Sortable;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<?php
$groups          = PropertyGroup::find()->all();
$issetProperties = ArrayHelper::map($modelProps, 'id', 'name');
?>

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <h2><?= Yii::t('taxonomy', 'All properties') ?></h2>

                    <div id="propertiesList" class="list-group">

                        <?php foreach ($groups as $group) : ?>
                            <h3 class="no-sorted"><?= $group->name ?></h3>
                            <?php foreach ($group->properties as $property) : ?>

                                <?php if (!isset($issetProperties[$property->id])) : ?>
                                    <?= $this->render('items/_propItem',
                                        ['model' => $model, 'property' => $property]) ?>
                                <?php endif ?>

                            <?php endforeach ?>

                        <?php endforeach ?>

                    </div>

                </div>
                <div class="col-md-6">
                    <h2><?= Yii::t('taxonomy', 'Owned properties') ?></h2>

                    <div id="propertiesListLinked" class="list-group">

                        <?php foreach ($modelProps as $property) : ?>

                            <?php if (isset($issetProperties[$property->id])) : ?>
                                <?= $this->render('items/_propItem',
                                    ['model' => $model, 'property' => $property]) ?>
                            <?php endif ?>

                        <?php endforeach ?>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <h2><?= Yii::t('taxonomy', 'All Values') ?></h2>
                    <?php Pjax::begin(['id' => 'properties-values']); ?>

                    <?php foreach ($modelProps as $property) : ?>

                        <?php if (isset($issetProperties[$property->id])) : ?>
                            <?= $this->render('items/_propItemValues',
                                ['model' => $model, 'property' => $property]) ?>
                        <?php endif ?>

                    <?php endforeach ?>

                    <?php Pjax::end(); ?>
                </div>
                <div class="col-md-6">
                    <h2><?= Yii::t('taxonomy', 'Owned Values') ?></h2>
                </div>
            </div>
        </div>
    </div>

<?php
$linkPropertyUrl = Url::toRoute('/taxonomy/property-value/link-property');
$linkVariatedUrl = Url::toRoute('/taxonomy/property-value/variated');
Sortable::widget([
    'disableSorted' => '.no-sorted',
    'pluginOptions' => [
        'id'          => 'propertiesList',
        'connectWith' => '.list-group',
        'sort'        => true,
    ],
]);
Sortable::widget([
    'events'        => [
        'receive' => <<<JS
            function (event, ui) {
                if ($(event.target).attr('id') === 'propertiesListLinked') {
                    var params = {
                        id: $(ui.item).data('id'),
                        object_id: $(ui.item).data('object_id')
                    };
                    var addeds = $('#propertiesListLinked .list-group-item');
                    var ids = [];
                    addeds.each(function(){
                        ids.push($(this).data('id'));
                    });
                    $.pjax({container:"#properties-values", "timeout" : 0, push:false, replace:false,"data":{"ids":JSON.stringify(ids)}});
                }
            }
JS
        ,
    ],
    'pluginOptions' => [
        'id'                 => 'propertiesListLinked',
        'connectWith'        => '.list-group',
        'connectplaceholder' => 'ui-state-highlight',
        'sort'               => true,
    ],
]);
$object = UrlHelper::classEncode($model::className());
$js     = <<<JS
    $('body').on('change', '.owned-values', function(){
        var checkbox = $(this);
        var check = checkbox.is(':checked');
        var params = {
                        property_id: checkbox.data('property_id'),
                        object_id: checkbox.data('object_id'),
                        value_id: checkbox.data('value_id'),
                        link: Number(check),
                        object: encodeURIComponent('{$object}')
                    };
        ajax('{$linkPropertyUrl}', params, false, false);
    });

    $('body').on('change', '.prop-variated', function(){
        var checkbox = $(this);
        var check = checkbox.is(':checked');
        var params = {
                        property_id: checkbox.data('property_id'),
                        object_id: checkbox.data('object_id'),
                        link: Number(check),
                        object: encodeURIComponent('{$object}')
                    };
        ajax('{$linkVariatedUrl}', params, false, false);
    });
JS;

$this->registerJs($js);