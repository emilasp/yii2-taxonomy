<?php
use emilasp\variety\models\Variety;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
?>

<?php
$variation = false;
if ($model->type === Variety::getValue('product_type_variate_pseudo')) {
    $variation = ($model->variation_id === $property->id);
}

?>
<div class="list-group-item" data-id="<?= $property->id ?>" data-object="<?= $model->id ?>">
    <div class="row">
        <div class="col-md-7">
            <span class="glyphicon glyphicon-move" aria-hidden="true"></span>
            <strong><?= $property->name ?></strong>
        </div>
        <div class="col-md-5">
            <?= Html::checkbox('variable' . $property->id, $variation, [
                'id' => 'variable' . $property->id,
                'class' => 'prop-variated modern-checkbox',
                'data'  => [
                    'object_id'  => $model->id,
                    'property_id' => $property->id,
                ],
            ]) ?>
            <label for="<?= 'variable' . $property->id ?>" class="modern-before">
                &nbsp;&nbsp;Вариация
            </label>
        </div>
    </div>


    <?php foreach ($property->values as $value) : ?>

        <?php $objectValues = array_keys(ArrayHelper::map($model->values, 'id', 'value')); ?>
        <div>
            <?= Html::checkbox('prop' . $value->id, in_array($value->id, $objectValues), [
                'class' => 'owned-values modern-checkbox',
                'id'    => 'prop' . $value->id,
                'data'  => [
                    'object_id'  => $model->id,
                    'property_id' => $property->id,
                    'value_id'    => $value->id,
                ],
            ]) ?>
            <label for="<?= 'prop' . $value->id ?>" class="modern-before">
                &nbsp;&nbsp;<?= $value->value ?>
            </label>
        </div>
    <?php endforeach ?>

</div>