<div class="list-group-item" data-id="<?= $property->id ?>" data-object="<?= $model->id ?>">
    <span class="badge"><?= count($property->values) ?></span>
    <span class="glyphicon glyphicon-move" aria-hidden="true"></span>
    <?= $property->name ?>
</div>