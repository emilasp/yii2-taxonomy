<?php
namespace emilasp\taxonomy\extensions\PropsValueObjManager;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use emilasp\taxonomy\models\Property;
use emilasp\core\components\base\Widget;

/** Добавляем управление привязкой значений свойств для объека
 * Class PropsValueObjManager
 * @package emilasp\taxonomy\extensions\PropsValueObjManager
 */
class PropsValueObjManager extends Widget
{
    /** @var string модель для которой управляем значениями свойств */
    public $object;

    public $enableVariation = false;
    private $objectProps;

    public function init()
    {
        parent::init();
        $this->addAddedModelProps();
        $this->addVariationProps();
    }

    public function run()
    {
        echo $this->render('manager', [
            'model'      => $this->object,
            'modelProps' => $this->objectProps,
        ]);
    }

    /** Суммируем свойства продукта и переданные свойства
     * @return array
     */
    private function addAddedModelProps()
    {
        $this->objectProps = $this->object->properties;
        $modelPropsIds     = array_keys(ArrayHelper::map($this->objectProps, 'id', 'id'));
        $addedProps        = json_decode(Yii::$app->request->get('ids'));
        if ($addedProps && is_array($addedProps)) {
            foreach ($addedProps as $propId) {
                if (!in_array($propId, $modelPropsIds)) {
                    $this->objectProps[] = Property::findOne($propId);
                }
            }
        }
    }

    /**
     * Добавляем в список свойств товара свойство с псеводо вариацией
     */
    private function addVariationProps()
    {
        if ($this->enableVariation) {
            $modelPropsIds   = array_keys(ArrayHelper::map($this->objectProps, 'id', 'id'));
            $modelVariations = $this->object->variations;

            if ($modelVariations && is_array($modelVariations)) {
                $modelVariationsIds = array_keys(ArrayHelper::map($modelVariations, 'id', 'id'));
                foreach ($modelVariationsIds as $propId) {
                    if (!in_array($propId, $modelPropsIds)) {
                        $this->objectProps[] = Property::findOne($propId);
                    }
                }
            }
        }
    }
}
