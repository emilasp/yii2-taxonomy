<?php
namespace emilasp\taxonomy\extensions\nestedTree;

use yii\web\AssetBundle;

/**
 * Class NestedTreeAsset
 * @package emilasp\taxonomy\extensions\nestedTree
 */
class NestedTreeAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $js  = ['nestedtree.js'];
    public $css = ['nestedtree.css'];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'emilasp\core\assets\FancyTreeAsset',
    ];
}
