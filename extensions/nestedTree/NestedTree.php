<?php
namespace emilasp\taxonomy\extensions\nestedTree;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use emilasp\taxonomy\models\Category;

/**
 *  NestedTree::widget([
 *      'id' => 'tree',
 *      'rootId' => 279,
 *      'options' =>[
 *          'id' => 'tree',
 *          'extensions' => ['dnd'],
 *          'dnd' => [
 *              'preventVoidMoves' => true,
 *              'preventRecursiveMoves' => true,
 *              'autoExpandMS' => 400,
 *              'dragStart' => new \yii\web\JsExpression('function(node, data) {
 *                      return true;
 *                  }'),
 *              'dragEnter' => new \yii\web\JsExpression('function(node, data) {
 *  				   return true;
 *  				}'),
 *              'dragDrop' => new \yii\web\JsExpression('function(node, data) {
 *                      data.otherNode.moveTo(node, data.hitMode);
 *                      $.ajax({
 *                          type: "POST",
 *                          url: "/taxonomy/category/move-node",
 *                          dataType: "json",
 *                          data: "node="+data.otherNode.key+"&nodeTarget="+data.node.key+"&mode="+data.hitMode,
 *                          success: function(msg) {
 *                              if(msg[0]=="1"){
 *                                  notice(msg[1], "green");
 *                              }else{
 *                                  notice(msg[1], "red");
 *                              }
 *                          },
 *                          error: function(){}
 *                      });
 *                  }'),
 *          ],
 *      ]
 *  ]);
 *
 * Class NestedTree
 * @package emilasp\taxonomy\extensions\nestedTree
 */
class NestedTree extends \yii\base\Widget
{
    /** @var string рутовый итем */
    public $rootId;

    /** @var array  */
    public $options = [];

    /** @var array  */
    private $defaultOptions = [
        'id' => 'tree',
    ];

    public function init()
    {
        parent::init();
        $this->defaultOptions = ArrayHelper::merge($this->defaultOptions, $this->options);
        $this->registerAssets();
    }

    public function run()
    {
        $items = $this->getChildsRoot();
        $arrays = $this->prepareItems($items);
        echo $this->renderTree($arrays);
    }

    /**
     * Registers the needed assets
     */
    protected function registerAssets()
    {
        NestedTreeAsset::register($this->view);
        $options = Json::encode($this->defaultOptions);
        $this->view->registerJs("\n\$(\"#{$this->options['id']}\").fancytree({$options});\n");
    }

    /** Преобразовываем nested set в массив с иерархией
     * @param $items
     *
     * @return array
     */
    private function prepareItems($items)
    {
        $stack = [];
        $arraySet = [];
        foreach ($items as $item) {
            $stackSize = count($stack);
            while ($stackSize > 0 && $stack[$stackSize - 1]['rgt'] < $item->lft) {
                array_pop($stack);
                $stackSize--;
            }
            $link =& $arraySet;
            for ($i = 0; $i < $stackSize; $i++) {
                $link =& $link[$stack[$i]['index']]['children']; //navigate to the proper children array
            }
            $tmp = array_push($link, [
                'id' => $item->getPrimaryKey(),
                'name' => $item->name,
                'children' => []
            ]);
            array_push($stack, [
                'index' => $tmp - 1,
                'rgt' => $item->rgt
            ]);
        }
        return $arraySet;
    }


    /** Получаем детей от рута
     * @return array
     */
    private function getChildsRoot()
    {
        $root = Category::findOne($this->rootId);
        if ($root) {
            return $root->children()->all();
        }
        return [];
    }

    /** Формируем дерево списков в html
     * @param $items
     *
     * @return string
     */
    private function renderTree($items)
    {
        $html = Html::beginTag('div', ['id' => $this->options['id']]);

        $html .= $this->renderItemsRecursive($items);
        $html .= Html::endTag('div');
        return $html;
    }

    /** Формируем списки рекурсивно
     * @param $items
     *
     * @return string
     */
    private function renderItemsRecursive($items)
    {
        $html = Html::beginTag('ul');
        foreach ($items as $item) {
            $html .= Html::beginTag('li', ['id' => $item['id']]);
            $html .= $item['name'];

            if (isset($item['children'])) {
                $html .= $this->renderItemsRecursive($item['children']);
            }

            $html .= Html::endTag('li');
        }
        $html .= Html::endTag('ul');
        return $html;
    }
}
