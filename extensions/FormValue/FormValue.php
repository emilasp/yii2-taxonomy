<?php
namespace emilasp\taxonomy\extensions\FormValue;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use emilasp\taxonomy\models\PropertyValue;

/**
 * Class FormValue
 * @package emilasp\taxonomy\extensions\FormValue
 */
class FormValue extends Widget
{
    /** @var string рутовый итем */
    public $propertyId;

    public $model;

    public function init()
    {
        parent::init();
        if (!$this->model) {
            $this->model = new PropertyValue();
        }
        $this->model->property_id = $this->propertyId;
    }

    public function run()
    {
        echo $this->render('_formValue', [
            'model' => $this->model,
            'property_id' => $this->propertyId,
        ]);
    }
}
