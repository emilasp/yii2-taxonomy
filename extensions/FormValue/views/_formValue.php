<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\taxonomy\models\PropertyValue */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="property-value-form" style="display: none">

        <?php $form = ActiveForm::begin(['id' => 'form-property-value']); ?>

        <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'property_id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'order')->textInput(['maxlength' => true]) ?>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'data')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('site', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>


<?php
$urlToDeleteValue = Url::toRoute(['/taxonomy/property-value/delete-ajax']);

$js = <<<JS

    $('body').on('click', '.value-delete', function() {
       if (confirm('Delete value?')) {
            var button = $(this);
            var id = button.data('id');
            $.ajax({
                url: '{$urlToDeleteValue}',
                type: 'post',
                dataType: "json",
                data: 'id=' + id,
                success: function(data) {
                    if(data[0]==1) {
                        notice(data[1], 'green');
                        $.pjax({
                            container:"#pjax-value-list",
                            "timeout" : 0,
                            push:false
                        });
                        $('#jBox-overlay').click();
                    } else {
                        notice(data[1], 'red');
                    }
                }
            });
        }
    });

    $('body').on('click', '.value-update', function() {
        var button = $(this);
        var id = button.data('id');
        var propertyid = button.data('propertyid');
        var value = button.data('value');
        var order = button.data('order');
        var data = button.data('data');

        $('#propList-add').click();

        $('#propertyvalue-id').val(id);
        $('#propertyvalue-value').val(value);
        $('#propertyvalue-data').val(data);
        $('#propertyvalue-order').val(order);
        $('#propertyvalue-property_id').val(propertyid);
    });


    $('#form-property-value').on('beforeSubmit', function(event, jqXHR, settings) {
        var form = $(this);
        if(form.find('.has-error').length) {
            return false;
        }

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            dataType: "json",
            data: form.serialize(),
            success: function(data) {
             console.log(data[0]);
                if(data[0]==1) {
                    notice(data[1], 'green');
                    $.pjax({
                        container:"#pjax-value-list",
                        "timeout" : 0,
                        push:false
                    });
                    $('#jBox-overlay').click();
                } else {
                    notice(data[1], 'red');
                }
            }
        });

        return false;
    })
JS;
$this->registerJs($js);